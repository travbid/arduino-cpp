#include <Arduino.h>

#include <SoftwareSerial.h>

SoftwareSerial hc06(2, 3);

void setup() {
	pinMode(7, OUTPUT);
	digitalWrite(7, LOW);

	Serial.begin(115200); // set baud rate
	hc06.begin(9600);
}

void loop() {

	while (hc06.available()) {
		char c = hc06.read();
		if (c == 'a') {
			digitalWrite(7, HIGH);
		} else if (c == 'b') {
			digitalWrite(7, LOW);
		} else if (c == 'z') {
			digitalWrite(7, LOW);
			exit(0);
		}
		Serial.write(c);
	}

	// Write from Serial Monitor to HC06
	if (Serial.available()) {
		char c = Serial.read();
		hc06.write(c);
	}

	delay(10);
}