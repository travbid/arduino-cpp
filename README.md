# Arduino using C++

Work-in-progress-y

This project is a personal template for programming Arduino without using the Arduino IDE.

You will have to edit `push.sh` for your Arduino device.
Also set your HOME folder at the top of `build.ninja`.

Tested with Arduino UNO.

## Install
- Arduino IDE: https://arduino.cc/en/Main/Software
- Ninja: https://ninja-build.org

Ubuntu:
```bash
sudo apt install arduino-core
```
Arch:
```bash
pacman -S arduino-avr-core
```

## Build
```ninja```

## Run
```./push.sh```
